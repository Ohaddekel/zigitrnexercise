import { createSlice } from '@reduxjs/toolkit';
import { getCustomerInfoThunk, getCustomerMainDataThunk } from './CustomerThunk';

export const CustomersSlice = createSlice({
  name: 'customer',
  initialState: {
    phone: '',
    token: '',
    benefits: '',
    customerInfo: '',
    mainData: '',
    error: '',
  },
  reducers: {
  },

  extraReducers: {
    [getCustomerInfoThunk.rejected.toString()]: (state, action) => {
      state.error = action.payload;
      return state.error;
    },
    [getCustomerInfoThunk.fulfilled.toString()]: (state, action) => {
      state.customerInfo = action.payload;
    },
    [getCustomerMainDataThunk.rejected.toString()]: (state, action) => {
      state.error = action.payload;
      return state.error;
    },
    [getCustomerMainDataThunk.fulfilled.toString()]: (state, action) => {
      state.mainData = action.payload;
    },
  }
});

export default CustomersSlice.reducer;