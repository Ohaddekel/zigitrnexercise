import { ApiService } from '../../Services/API/API';


export interface ICustomerService {
    getCustomerInfo: () => Promise<any>;
    getCustomerMainData: () => Promise<any>;
}

export const CustomerService: ICustomerService = {
    getCustomerInfo: () => {
        return ApiService.makeRequest("GET", `/customers/info`)
    },

    getCustomerMainData: () => {
        return ApiService.makeRequest("GET", `/customers/main-page`)
    },

}

