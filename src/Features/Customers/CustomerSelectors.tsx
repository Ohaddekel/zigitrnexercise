export const selectCustomerInfo = (state) => state.customer.customerInfo;
export const selectCustomerMainData = (state) => state.customer.mainData;
export const selectCustomerBenefits = (state) => state.customer.benefits;

