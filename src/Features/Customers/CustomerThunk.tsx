import { createAsyncThunk } from '@reduxjs/toolkit';
import { CustomerService } from "./CustomerService";

export const getCustomerInfoThunk = createAsyncThunk(
    'customer/getCustomerInfo',
    async () => {
        try {
            const res = await CustomerService.getCustomerInfo();
            if (res && res.data && res.data.data)
                return res.data.data;
        } catch (error) {
            return (error);
        }
    }
);

export const getCustomerMainDataThunk = createAsyncThunk(
    'customer/getCustomerMainData',
    async () => {
        try {
            const res = await CustomerService.getCustomerMainData();
            if (res && res.data && res.data.data)
            return res.data.data;
        } catch (error) {
            return (error);
        }
    }
);











