import { createAsyncThunk } from '@reduxjs/toolkit';
import { ProductsService } from "./ProductService";

export const getProductsThunk = createAsyncThunk(
    'product/getProducts',
    async () => {
        try {
            const res = await ProductsService.getProducts();
            if (res && res.data && res.data.data) {
                return res.data.data;
            }
            return ('error,cant getProducts ');
        } catch (error) {
            return (error);
        }
    }
);











