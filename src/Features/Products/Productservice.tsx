import { ApiService } from '../../Services/API/API';

export const ProductsService = {
    getProducts: () => {
        return ApiService.makeRequest("GET", `/categories`)
    },
}


