import { createSlice } from '@reduxjs/toolkit';
import { SimpleProduct } from '../../Models';
import { getProductsThunk } from "./ProductsThunk";
export const ProductsSlice = createSlice({
  name: 'product',
  initialState: {
    customerFavoriteProducts: [],
    products: [],
    error: '',
  },
  reducers: {
    addProductToFavoriteProducts: (state, action) => {
      try {
        if (action.payload) {
          if (!state.customerFavoriteProducts.find(item => item.id === action.payload.id)) {
            state.customerFavoriteProducts.push(action.payload);
          }
        }
      }
      catch (action) {
        return action.payload;
      }
    },
    removeProductFromFavoriteProducts: (state, action) => {
      if (action.payload) {
        state.customerFavoriteProducts = state.customerFavoriteProducts.filter(item => item.id !== action.payload.id);
      }
    },
  },
  extraReducers: {
    [getProductsThunk.rejected.toString()]: (state, action) => {
      state.error = action.payload;
      return state.error;
    },
    [getProductsThunk.fulfilled.toString()]: (state, action) => {
      state.products = action.payload;
    },
  }
});

export const { addProductToFavoriteProducts, removeProductFromFavoriteProducts } = ProductsSlice.actions;

export default ProductsSlice.reducer;

