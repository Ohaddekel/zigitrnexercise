export const selectCustomerFavoriteProducts = (state) => state.product.customerFavoriteProducts;
export const selectProducts = (state) => state.product.products;