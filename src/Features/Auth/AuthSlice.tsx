import { createSlice } from '@reduxjs/toolkit';
import { otpRequestThunk, otpVerificationThunk } from "./AuthThunk";

export const AuthSlice = createSlice({
  name: 'authentication',
  initialState: {
    phoneNumber: '',
    token: '',
    availableMemberPoints: 0,
    completedOnBoarding: false,
    error: '',
  },
  reducers: {
  },
  extraReducers: {
    [otpRequestThunk.rejected.toString()]: (state, action) => {
      state.error = 'otpRequest failed';
      return state.error;
    },
    [otpRequestThunk.fulfilled.toString()]: (state, action) => {
      if (action.payload) {
        debugger;
        state.phoneNumber = action.payload;
        state.completedOnBoarding = true;
      }
    },
    [otpVerificationThunk.rejected.toString()]: (state, action) => {
      state.error = 'otpVerification failed';
      return state.error;
    },
    [otpVerificationThunk.fulfilled.toString()]: (state, action) => {
      state.token = action.payload;
    },
  }
});

export default AuthSlice.reducer;