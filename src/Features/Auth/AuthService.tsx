import { ApiService } from '../../Services/API/API';

export interface IAuthService {
    otpRequest: (phoneNumber: any) => Promise<any>;
    otpVerification: (phoneNumber: any, phoneCode: any) => Promise<any>;
}
export const AuthService: IAuthService = {
    otpRequest: (phoneNumber: any) => {
        return ApiService.makeRequest("GET", `/customers/otp?phone=${phoneNumber}`)
    },
    otpVerification: (phoneNumber: any, phoneCode: any) => {
        return ApiService.makeRequest("GET", `/customers/validate?phone=${phoneNumber}&code=${phoneCode}`)
    }
}



