import { createAsyncThunk } from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { AuthService } from "./AuthService";

export const otpRequestThunk = createAsyncThunk(
    'authentication/otpRequest',
    async (phoneNumber: string, { rejectWithValue }) => {
        try {
            const res = await AuthService.otpRequest(phoneNumber);
            if (res && res.data && res.data.data) {
                return phoneNumber;
            }
            return rejectWithValue('error');
        } catch (error) {
            return rejectWithValue('error');
        }
    }
);

export const otpVerificationThunk = createAsyncThunk(
    'authentication/otpVerification',
    async (phoneCode, { rejectWithValue, getState }) => {
        try {
            const state = getState();
            const res = await AuthService.otpVerification(state.auth.phoneNumber, phoneCode);
            if (res && res.data && res.data.data) {
                await AsyncStorage.setItem('@userToken', res.data.data.token);
                return res.data.data.token;
            }
            return rejectWithValue('error');
        } catch (error) {
            return rejectWithValue('error');
        }
    }
);









