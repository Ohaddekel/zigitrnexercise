export const selectPhoneNumber = (state) => state.auth.phoneNumber;
export const selectToken = (state) => state.auth.token;
export const selectAvailableMemberPoints = (state) => state.auth.availableMemberPoints;
export const selectCompletedOnBoarding = (state) => state.auth.completedOnBoarding;
