import React from 'react';
import HomeScreen from '../Screens/HomePage/HomePage';
import LoginScreen from '../Screens/Login/LoginScreen';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { useTranslation } from 'react-i18next';

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
    const { t } = useTranslation();

    return (
        <Drawer.Navigator initialRouteName="Home" drawerPosition="right">
            <Drawer.Screen
                name={'Home'}
                component={HomeScreen}
                options={() => ({
                    title: t('homeScreen'),
                })}
            />
            <Drawer.Screen
                name={'Login'}
                component={LoginScreen}
                options={() => ({
                    title: t('loginScreen'),
                })}
            />
        </Drawer.Navigator>
    );
};

export default DrawerNavigator;
