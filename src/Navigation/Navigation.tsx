

// const Feed = () => {
//     return (
//         <Text>Feed Screen</Text>
//     )
// }

// const Article = () => {
//     return (
//         <Text>Article Screen</Text>
//     )
// }

// function MyDrawer() {
//     return (
//         <Drawer.Navigator>
//             <Drawer.Screen name="Feed" component={Feed} />
//             <Drawer.Screen name="Article" component={Article} />
//         </Drawer.Navigator>
//     );
// }

// export default MyDrawer;

import React, { useRef, useState } from "react";
import { Button, DrawerLayoutAndroid, Text, StyleSheet, View, Image, TouchableOpacity } from "react-native";
import HomeScreen from "../Screens/HomePage/HomePage";
import TopMenuNav from "../Components/TopMenuNav/TopMenuNav";
import FavIcon from "../../assets/Icons/icon-favorite";

const iconMenu = require("../../assets/Icons/icon-menu.png");


import Welcome from "../Screens/Welcome/Welcome";
import LoginScreen from "../Screens/Login/LoginScreen";
import { createDrawerNavigator } from '@react-navigation/drawer';
const Drawer = createDrawerNavigator();


import styles from "./NavigationStyle";
const Navigation = () => {
    const drawer = useRef(null);
    const navigationView = () => (
        <Drawer.Navigator>
            <Drawer.Screen name="Home" component={HomeScreen} />
            <Drawer.Screen name="Welcome" component={Welcome} />
            <Drawer.Screen name="LoginScreen" component={LoginScreen} />
        </Drawer.Navigator>
    );
    return (
        <DrawerLayoutAndroid
            ref={drawer}
            drawerWidth={300}
            drawerPosition={"right"}
            renderNavigationView={navigationView}
        >
            <View style={styles.container}>
                <View>
                    <TouchableOpacity onPress={() => drawer.current.openDrawer()}>
                        <Image
                            source={iconMenu}
                            style={styles.image}
                        />
                    </TouchableOpacity>
                </View>
            </View>


        </DrawerLayoutAndroid>
    );
};


export default Navigation;