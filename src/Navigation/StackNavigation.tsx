import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import WelcomeScreen from '../Screens/Welcome/Welcome';
import LoginScreen from '../Screens/Login/LoginScreen';
import HomeScreen from '../Screens/HomePage/HomePage';
import ProductScreen from '../Screens/Product/ProudctPage';
import ProductsCustomizationScreen from '../Screens/ProductCustomization/ProductCustomizationScreen';

import { getCustomerInfoThunk, getCustomerMainDataThunk } from "../Features/Customers/CustomerThunk";
import { getProductsThunk } from "../Features/Products/ProductsThunk";

import { useDispatch } from 'react-redux';
const Stack = createStackNavigator();

function StackNavigation() {
    const dispatch = useDispatch();
    dispatch(getCustomerInfoThunk());
    dispatch(getCustomerMainDataThunk());
    dispatch(getProductsThunk());
    return (
        <NavigationContainer >
            <Stack.Navigator initialRouteName="Home" screenOptions={{ headerShown: false }}>
                <Stack.Screen name="Welcome" component={WelcomeScreen} />
                <Stack.Screen name="Login" component={LoginScreen} />
                <Stack.Screen name="Home" component={HomeScreen} />
                <Stack.Screen name="Product" component={ProductScreen} />
                <Stack.Screen name="ProductCustomization" component={ProductsCustomizationScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default StackNavigation;