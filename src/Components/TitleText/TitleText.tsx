import React from 'react';
import { Text } from 'react-native';
import styles from "./TitleTextStyles";

const TitleText = props => (
    <Text style={{ ...styles.input, ...props.style }}>{props.children}</Text>
);

export default TitleText;
