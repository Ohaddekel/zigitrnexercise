import React from 'react'
import { View, Text, Image, TouchableOpacity } from "react-native"
const productImage = require('../../../assets/product-screenshot.png');
import styles from "./GalleryStyles";
import { SimpleProduct } from '../../Models';

interface CarouselItemProps {
    item: SimpleProduct;
    onPress: () => void;
}

const Pressed = () => {
    console.log('item was pressed');
}
const GalleryItem = ({ item, onPress }: CarouselItemProps) => {
    const { name } = item
    return (
        <TouchableOpacity style={styles.item} onPress={Pressed}>
            <Image
                source={productImage}
                style={styles.itemImage}
            />
            <View style={styles.itemTextCenter}>
                <Text style={styles.itemTitle}>{name}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default GalleryItem;
