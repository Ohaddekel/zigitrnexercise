import { StyleSheet } from 'react-native';
import colors from "./../../Constants/Colors";
export default StyleSheet.create({
    item: {
        marginLeft: 20,
        height: 250,
        width: 148,
        backgroundColor: colors.primary,
        borderRadius: 10,
    },
    itemTextCenter: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemImage: {
        resizeMode: 'contain',
        width: 130,
        height: 150,
        marginLeft: 15,
        marginBottom: 2,
    },
    itemTitle: {
        color: "#6CA93F",
        fontSize: 18,
        paddingLeft: 10,
    },
    ItemBody: {
        color: "#222",
        fontSize: 18,
        paddingLeft: 20,
        paddingRight: 20,
    },
});








