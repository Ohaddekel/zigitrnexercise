import React from "react";
import { FlatList, SafeAreaView } from "react-native";
import GalleryItem from "./GalleryItem";
const RecommendedProductsGallery = ({ products }) => {
    const { recommendations } = products;

    return (
        <SafeAreaView>
            <FlatList
                data={recommendations}
                renderItem={({ item }) => <GalleryItem
                    item={item}
                    onPress={() => { }}
                />}
                keyExtractor={(item) => item.id}
                horizontal={true}
            />
        </SafeAreaView>
    );
};



export default RecommendedProductsGallery;