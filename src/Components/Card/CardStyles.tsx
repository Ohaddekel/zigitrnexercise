import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
    card: {
        height: 209,
        width: 180,
    },
});

