import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
    GrettingContainer: {
        backgroundColor: colors.primary,
        flexDirection: 'row',
        padding: 55,
        borderBottomRightRadius: 30,
        borderBottomLeftRadius: 30,
    },
    GrettingImage: {
        backgroundColor: 'pink',
        justifyContent: 'flex-end',
    },
    GrettingTitle: {
        marginTop: 20,
        fontSize: 20,
    },
});

