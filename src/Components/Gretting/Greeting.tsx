import React from "react";
import { Text, View, Image } from "react-native";
import { useSelector } from 'react-redux';
import { selectAvailableMemberPoints } from "../../Features/Auth/AuthSelectors";
import GrettingImg from "../../../assets/Icons/greeting-good-afternoon";
import styles from "./GreetingStyles";
const Gretting = () => {
    const availableMemberPoints = useSelector(selectAvailableMemberPoints);
    return (
        <View style={styles.GrettingContainer}>
            <Image
                source={require('../../../assets/image-greeting.png')}
                style={styles.GrettingImage}
            />
            <View>
                <GrettingImg style={styles.GrettingTitle} />
                <Text style={styles.GrettingTitle}> צברת עד היום {availableMemberPoints} Goodies</Text>
            </View>

        </View>
    );
};
export default Gretting;