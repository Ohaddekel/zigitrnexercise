import React from "react";
import { FlatList, SafeAreaView } from "react-native";
import CarouselItem from "./CarouselCardItem";
const Carousel = ({ products }) => {
    const { recommendations } = products;
    return (
        <SafeAreaView>
            <FlatList
                data={recommendations}
                renderItem={({ item }) => <CarouselItem
                    item={item}
                    onPress={() => { }}
                />}
                keyExtractor={(item) => item.id}
                horizontal={true}
            />
        </SafeAreaView>
    );
};



export default Carousel;