import React, { useMemo, useState } from 'react'
import { View, Text, Image, TouchableOpacity } from "react-native"
import FavoriteIcon from "../../../assets/Icons/icon-favorite";
const FavoriteActiveIcon = require("../../../assets/Icons/activeFavorite");
const productImage = require('../../../assets/product-screenshot.png');
import { useSelector, useDispatch } from 'react-redux';
import styles from "./CarouselStyles";
import { addProductToFavoriteProducts, removeProductFromFavoriteProducts } from "../../Features/Products/ProductsSlice";
import { selectCustomerFavoriteProducts } from '../../Features/Products/ProductsSelectors';
import { SimpleProduct } from '../../Models';
import Tooltip from "../Tooltip/Tooltip";

interface CarouselItemProps {
    item: SimpleProduct;
    onPress: () => void;
}

const CarouselItem = ({ item, onPress }: CarouselItemProps) => {
    const dispatch = useDispatch();

    const fav = useSelector(selectCustomerFavoriteProducts);
    const { name, description, image } = item
    const isFavorite = useMemo(() => fav.find((f: any) => f.id === item.id), [fav, item]);

    const toggleFavorites = () => {

        if (isFavorite) {
            dispatch(removeProductFromFavoriteProducts(item));

        }
        else {
            dispatch(addProductToFavoriteProducts(item));
        }
    }
    return (
        <TouchableOpacity style={styles.item} onPress={onPress}>
            <Image
                source={{
                    uri: image,
                }}
                style={styles.itemImage}
            />
            <View style={styles.itemTextCenter}>
                <Text style={styles.itemTitle}>{name}</Text>
            </View>

            <View style={styles.itemIcons}>
                <Tooltip description={description} />
                <TouchableOpacity
                    onPress={() => { toggleFavorites() }}
                >
                    {!isFavorite && <FavoriteIcon />}
                    {isFavorite && <Text>Im Favorite!</Text>}
                </TouchableOpacity>

            </View>

        </TouchableOpacity>
    )
}

export default CarouselItem;