import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
    separator: {
        fontSize: 20,
        marginHorizontal: 12,
        borderBottomColor: colors.secondary,
        borderBottomWidth: 1.2,
        width: 18,
        fontWeight: 'bold',
    },
});


