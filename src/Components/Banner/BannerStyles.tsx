
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    bannerContainer: {
        width: "95%",
    },

    banner: {
        width: '100%',
        height: 150,
    },
});


