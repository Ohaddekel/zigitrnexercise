import React from 'react';
import { View, Image, Text } from 'react-native';
import styles from "./BannerStyles";
const BannerImg = require("../../../assets/banner.png");


const Banner = () => {
  return (
    <View style={styles.bannerContainer}>
      <Image
        source={BannerImg}
        style={styles.banner}
        resizeMode="contain" />
    </View>
  );
};

export default Banner;
