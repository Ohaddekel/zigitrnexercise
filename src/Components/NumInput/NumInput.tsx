import React, { useState } from "react";
import { TouchableWithoutFeedback, KeyboardAvoidingView, View } from "react-native";
import Input from "../Input/Input";
import styles from "./NumInputStyles";

const NumInput = (props) => {
  const [enteredValue, setEnteredValue] = useState('');
  const numberInputHandler = inputText => {
    setEnteredValue(inputText.replace(/[^0-9]/g, ''));
  };

  return (
    <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={30}>
      <TouchableWithoutFeedback >
        <View>
          <Input
            style={styles.input}
            blurOnSubmit
            keyboardType="number-pad"
            maxLength={10}
            onChangeText={numberInputHandler}
            value={enteredValue}
            placeholder="הכנס מספר פלאפון"
          />
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>

  );
};
export default NumInput;