import React from 'react'
import { Controller } from 'react-hook-form';
import { View, TextInput, Text } from 'react-native'
import styles from "./TextInputStyles";

const DefaultTextInput = (props) => {
    const {
        control,
        placeholder,
        controllerName,
        defaultValue,
        errors,
        rules,
        errorMsg } = props
    const { ...rest } = props

    return (
        <View>
            <Controller
                control={control}
                render={({ onChange, onBlur, value }) => (
                    <TextInput
                        style={styles.textAreaInput}
                        onChangeText={text => onChange(text)}
                        placeholder={placeholder}
                        value={value}
                        multiline
                        editable
                        numberOfLines={6}
                        {...rest}
                    />
                )}
                name={controllerName}
                rules={rules}
                defaultValue={defaultValue}
            />
            {errors[props.controllerName] && <Text>{errorMsg}</Text>}

        </View>
    )
}

export default DefaultTextInput;