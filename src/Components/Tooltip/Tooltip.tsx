import React from "react";
import { Tooltip, Text } from 'react-native-elements';
import InfoIcon from "../../../assets/Icons/icon-info";
const TooltipItem = ({ description }) => {
    return (
        <Tooltip height={140} width={200} backgroundColor={"white"} popover={<Text>{description}</Text>}>
            <InfoIcon />
        </Tooltip>

    )
}

export default TooltipItem;