import React from "react";
import { View, Text } from "react-native";
import styles from "./ProudctPageStyles";
import { useSelector, useDispatch } from 'react-redux';
import { selectProducts } from "../../Features/Products/ProductsSelectors";

const ProductScreen = () => {
    const products = useSelector(selectProducts);
    return (
        <View>
            <Text style={styles.header}>Product Page</Text>
        </View>
    )
}

export default ProductScreen;