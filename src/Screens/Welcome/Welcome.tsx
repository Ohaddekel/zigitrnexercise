import React from "react";
import { View, Text, Button } from "react-native";
import styles from "./WelcomeStyles";

const Welcome = ({ navigation }) => {

    return (
        <View style={styles.container}>
            <Text style={styles.header}>כניסה</Text>
            <Button
                title="Go to Login Page"
                onPress={() =>
                    navigation.navigate('Login')
                }
            />
        </View>
    )
}

export default Welcome;