import React, { useEffect } from "react";
import { View, SafeAreaView, ScrollView } from "react-native";
import { useSelector, useDispatch } from 'react-redux';
import Carousel from "../../Components/Carousel/Carousel";
import TitleText from "../../Components/TitleText/TitleText";
import styles from "./HomePageStyles";
import Gretting from "../../Components/Gretting/Greeting";
import Banner from "../../Components/Banner/Banner";
import { selectCustomerMainData } from "../../Features/Customers/CustomerSelectors";
import RecommendedProducts from "../../Containers/RecommendedProducts/RecommendedProducts";
import RebarTop from "../../Containers/RebarTop/RebarTop";
import LastProducts from "../../Containers/LastProducts/LastProducts";
export default function HomeScreen() {
    const products = useSelector(selectCustomerMainData);
    const { latestProducts } = products;
    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.homeContainer}>
                    <Gretting />
                    <View style={styles.homeContainerBottom}>
                        {latestProducts ? <LastProducts /> : <RebarTop />}
                        <RecommendedProducts />
                    </View>
                    <Banner />
                </View>
            </ScrollView>
        </SafeAreaView>

    );
}





