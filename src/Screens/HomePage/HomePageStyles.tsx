import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
    homeContainer: {
        backgroundColor: colors.itemBackgroundColor,
    },
    homeContainerBottom: {
        flex: 1,
    },
    homeContainerBottomTitle: {
        marginLeft: 50,
        marginTop: 15,
        padding: 20,
        flex: 1,
    },
});

