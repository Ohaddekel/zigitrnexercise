
import React, { useEffect } from 'react';
import { View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import GeneralText from "../../Components/GeneralText/GeneralText";
import LoginForm from "../../Containers/LoginForm/LoginForm";
import styles from "./LoginScreenStyles";
import { useSelector } from 'react-redux';


function LoginScreen({ navigation }) {
    const { token } = useSelector(state => state.auth)

    useEffect(() => {
        if (token) {
            debugger;
            navigation.navigate('Home')
        }
    }, [token])
    const userIsAuth = async () => {
        debugger;
        const token = await AsyncStorage.getItem('@userToken');
        if (token) {
            navigation.navigate('Home')
        }
    }
    useEffect(() => {
        debugger;
        userIsAuth();
    }, [userIsAuth])

    return (
        <View style={styles.loginContainer}>
            <View style={styles.loginBody}>
                <GeneralText >
                    היי rebar friend
                    בואו נבדוק אם אנחנו כבר חברים
                </GeneralText>
                <LoginForm />
            </View>
        </View>
    )
}
export default LoginScreen




