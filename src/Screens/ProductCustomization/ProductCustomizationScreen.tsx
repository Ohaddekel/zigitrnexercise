import React from "react";
import { View, Text } from "react-native";
import styles from "./ProductCustomizationScreenStyles";
const ProductCustomizationScreen = () => {
    return (
        <View>
            <Text style={styles.header}>Product Customization Screen</Text>
        </View>
    )
}

export default ProductCustomizationScreen;