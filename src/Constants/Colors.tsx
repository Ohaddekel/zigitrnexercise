export default {
    primary: '#fff',
    secondary: '#000',
    placeHolder: '#808080',
    error: '#FF0000',
    borderColor: '#ababab',
    itemBackgroundColor: '#F4F4F9',
}


