const hebrewTranslations = {
  welcome: 'Hello',
  homeScreen: 'מסך הבית',
  profileScreen: 'אזור אישי',
  branchesScreen: 'סניפים',
  contactScreen: 'צור קשר',
  termsScreen: 'תקנון',
  buttons: {
    confirm: "אישור",
    continue: "המשך",
    done: "סיימתי",
  },
  onboarding: {
    re: " re",
    hey: " היי",
    welcome: "bar friend",
    OTP: {
      header: "בואו נבדוק אם אנחנו כבר חברים",
      phone: "הזן מספר הנייד",
      phoneCode: "הזן/י את הקוד שקיבלת לנייד",
      button: "המשך לקבלת קוד לנייד",
      CodeNotReceived: "קבלת הקוד ",
      CodeNotReceivedLink: "שלח שוב",
      CodeNotReceivedMsg: "שלחנו שוב מקווים שהפעם זה הצליח!",
    },
    BasicInfo: {
      before: " לפני שנכין את ה- rebar שלך",
      letsMeet: 'בואו נכיר אותך יותר טוב',
      firstName: "איך קוראים לך?",
      lastName: "שם משפחה",
      email: "מה המייל שלך?",
    },
    BirthCity: {
      header: "אנחנו ממש אוהבים לפנק",
      birthday: "מתי יש לך יום הולדת?",
    },
    ChooseTags: {
      whatYouLike: "מה את/ה אוהב/ת ב-rebar? ",
      choose: "בחר/י עד 6 קטגוריות שמתאימות לך",
      error: "אנא בחר/י בין 1-6 קטגוריות"
    },
  },
  contact: {
    message: "לשליחת הודעה",
    whatsAppHeader: "לפניות ניתן לייצור איתנו קשר בהודעת WhatsApp",
    formHeader: "ניתן גם לפנות אלינו באמצעות טופס לפניות",
    placeholders: {
      message: "...הקלד כאן את ההודעה",
      firstName: "...הקלד כאן שם פרטי",
      lastName: "...הקלד כאן שם משפחה",
      email: "...הקלד כאן טלפון",
      phone: "...הקלד כאן אימייל",
    },
    headers: {
      firstName: "שם פרטי",
      lastName: "שם משפחה",
      phone: "טלפון",
      email: "כתובת המייל שלך",
    },
    officeInfo: {
      street: "רחוב",
      zip: "מיקוד",
      drushim: "לפניות בנושא דרושים: jobs@rebar.co.il",
      mainOffice: "משרד ראשי",
    }
  }
};

export default hebrewTranslations;
