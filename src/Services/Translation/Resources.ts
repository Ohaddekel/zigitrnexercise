import hebrewTranslations from './HebrewTranslations';

export const resources = {
  hebrew: {
    translation: hebrewTranslations,
  },
};
