import axios, { Method } from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const _axios = axios.create({
    baseURL: `https://c36raxna13.execute-api.us-east-1.amazonaws.com/test/api`
});

_axios.interceptors.response.use(response => {
    if (response.data.data && response.data.data.token) {
        _axios.defaults.headers.common['Authorization'] = `Bearer ${response.data.data.token}`
    }
    return response;
}, function (error) {
    return Promise.reject(error);
});

_axios.interceptors.request.use(async function (config) {
    const token = await AsyncStorage.getItem('@userToken');
    config.headers.Authorization = token ? `Bearer ${token}` : '';
    return config;
});

export interface IApiService {
    makeRequest: (method: Method, url: string, data?: any) => Promise<any>;
}

export const ApiService: IApiService = {
    makeRequest: (method: Method, url: string, data?: any) => {
        return _axios({
            method,
            url,
            data
        }).catch(error => {
            console.log(error);
        });
    }
}