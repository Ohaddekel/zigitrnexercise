export default interface SimpleProduct {
    name: string;
    description: string;
    image: string;
    id: string
}