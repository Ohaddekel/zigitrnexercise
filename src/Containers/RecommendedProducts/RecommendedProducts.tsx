import React, { useEffect } from "react";
import { View, SafeAreaView, ScrollView } from "react-native";
import TitleText from "../../Components/TitleText/TitleText";
import { useSelector, useDispatch } from 'react-redux';
import Gallery from "../../Components/Gallery/Gallery";
import { selectCustomerMainData } from "../../Features/Customers/CustomerSelectors";
import styles from "./RecommendedProductsStyles";

const RecommendedProducts = () => {
    debugger;
    const products = useSelector(selectCustomerMainData);

    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.homeContainer}>
                    <View style={styles.homeContainerBottom}>
                        <TitleText style={styles.homeContainerBottomTitle}>מומלצים עבורך</TitleText>
                        <Gallery products={products} />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>

    );
}

export default RecommendedProducts;



