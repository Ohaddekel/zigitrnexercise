import React, { useState } from "react";
import { Text, View, TextInput } from "react-native";
import Button from "../../Components/Button/Button";
import { useForm, Controller } from "react-hook-form";
import { useDispatch } from 'react-redux';
import { otpRequestThunk } from "../../Features/Auth/AuthThunk";
import OTPVerification from "../OTPVerification/OTPVerification";
import styles from "./LoginFormStyles";

export default function LoginForm() {
  const dispatch = useDispatch();
  const { control, handleSubmit, formState: { errors } } = useForm();
  const [phoneNumber, setPhoneNumber] = useState(false);
  const [validOtpRequest, SetValidOtpRequest] = useState(false);

  const onSubmit = (data: any) => {
    const { phoneNumber } = data;

    if (phoneNumber) {
      dispatch(otpRequestThunk(phoneNumber));
      SetValidOtpRequest(true);
    }
  };
  return (
    <View>
      <Controller
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            onBlur={onBlur}
            onChangeText={value => onChange(value)}
            value={value}
            maxLength={10}
            keyboardType={'numeric'}
            onChange={() => { setPhoneNumber(value) }}
            style={styles.input}
          />
        )}
        name="phoneNumber"
        rules={{ required: true, minLength: 10, maxLength: 10, pattern: /[0-9]{10}/ }}
      />
      {phoneNumber
        ?
        <View style={styles.container}>
          <Text style={styles.phoneNumberPlaceHolder}>הזן מספר נייד</Text>
          <Button style={styles.button} title="המשך לקבלת קוד לנייד" onPress={handleSubmit(onSubmit)} />
        </View>

        :
        <View style={styles.container} >
          <Text style={styles.phoneNumberSecondary}>הזן מספר נייד</Text>
          <Button title="קוד ישלח לנייד שלך" style={styles.button} />
          <Button title="לא קיבלתי קוד שלח שוב" onPress={() => { SetValidOtpRequest(true) }} />
        </View>
      }
      {errors.phoneNumber && <Text style={styles.errorText}>הזן מספר תקין</Text>}
      {validOtpRequest
        ?
        <OTPVerification />
        : null
      }
    </View >
  );
}

