import React, { useEffect } from "react";
import { View, SafeAreaView, ScrollView } from "react-native";
import TitleText from "../../Components/TitleText/TitleText";
import { useSelector, useDispatch } from 'react-redux';
import Carousel from "./Carousel/Carousel";
import { selectCustomerMainData } from "../../Features/Customers/CustomerSelectors";
import styles from "../../Screens/HomePage/HomePageStyles";

const RecommendedProducts = () => {
    const products = useSelector(selectCustomerMainData);

    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.homeContainer}>
                    <View style={styles.homeContainerBottom}>
                        <TitleText style={styles.homeContainerBottomTitle}>ריבר טופ</TitleText>
                        <Carousel products={products} />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>

    );
}

export default RecommendedProducts;



