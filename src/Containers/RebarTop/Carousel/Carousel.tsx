import React from "react";
import { FlatList, SafeAreaView } from "react-native";
import CarouselItem from "./CarouselCardItem";
const Carousel = ({ products }) => {
    debugger;
    const { topRebar } = products;
    return (
        <SafeAreaView>
            <FlatList
                data={topRebar}
                renderItem={({ item }) => <CarouselItem
                    item={item}
                    onPress={() => { }}
                />}
                keyExtractor={(item) => item.id}
                horizontal={true}
            />
        </SafeAreaView>
    );
};



export default Carousel;