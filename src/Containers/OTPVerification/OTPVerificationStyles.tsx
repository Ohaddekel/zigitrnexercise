import { StyleSheet } from 'react-native';
import colors from "../../Constants/Colors";

export default StyleSheet.create({
    container: {
        marginTop: -300,
        justifyContent: 'center',
        alignItems: 'center',
    },
    controller: {
        backgroundColor: colors.primary,
    },
    ceneterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        color: 'green',
    },
    separatorContainer: {
        flexDirection: 'row',
        marginTop: 15,
    },
    text: {
        color: colors.secondary,
        fontSize: 20,
    },
    Buttom: {
        backgroundColor: 'pink',
        marginTop: 150,
    },
    Bottom: {
        marginTop: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    BottomText: {
        color: colors.primary,
        fontSize: 20,
    },
});

