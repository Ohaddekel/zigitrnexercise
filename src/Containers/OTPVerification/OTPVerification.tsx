import React, { useState, useEffect } from "react";
import { Text, View, TextInput } from "react-native";
import Button from "../../Components/Button/Button";
import { useForm, Controller } from "react-hook-form";
import { useDispatch } from 'react-redux';
import { otpVerificationThunk } from '../../Features/Auth/AuthThunk';

import styles from "./OTPVerificationStyles";

const OTPVerification = () => {
    const dispatch = useDispatch();
    const [otpRecieved, setOtpRecieved] = useState(true);
    const { control, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = async (data) => {
        debugger;
        const { phoneCode } = data;
        dispatch(otpVerificationThunk(phoneCode));
    }
    return (
        <View style={styles.container}>
            {otpRecieved ? <View>
                <Text style={styles.text}>הזן/י את הקוד שקיבלת לנייד</Text>
                <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            onBlur={onBlur}
                            onChangeText={value => onChange(value)}
                            value={value}
                            maxLength={4}
                            keyboardType="number-pad"
                            style={styles.controller}
                        />
                    )}
                    name="phoneCode"
                    rules={{ required: true, maxLength: 4, minLength: 4 }}
                />
                {errors.phoneCode && <Text style={styles.errorText}>קוד לא תקין</Text>}

                <Button title="לא קיבלתי קוד שלח שוב" onPress={() => { setOtpRecieved(false) }} />
                <Button title="המשך" onPress={handleSubmit(onSubmit)} />
            </View>
                : <View style={styles.Bottom}>
                    <Text style={styles.BottomText}>תצייץ/י בנייד נשלך אלייך קוד :)</Text>
                    <Text style={styles.BottomText}>שלחנו לך שוב סיסמא,מקווים  שהפעם הצליח :) </Text>
                </View>}
            {/* <View style={styles.separatorContainer}>
                <Separator />
                <Separator />
                <Separator />
                <Separator />
            </View> */}
            {/* <Text style={styles.BottomText}>תצייץ/י בנייד נשלח אלייך קוד</Text> */}
        </View >
    )
};


export default OTPVerification;

