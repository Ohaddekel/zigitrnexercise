import React from "react";
import { FlatList, SafeAreaView } from "react-native";
import CarouselItem from "./CarouselCardItem";
import LastProductsData from "../LastProductsData";

const Carousel = ({ products }) => {
    const { latestProducts, recommendations } = products;
    const uniqueProducts = LastProductsData.filter(product => !recommendations.includes(product));
    var a = uniqueProducts
    return (
        <SafeAreaView>
            <FlatList
                data={uniqueProducts}
                renderItem={({ item }) => <CarouselItem
                    item={item}
                    onPress={() => { }}
                />}
                keyExtractor={(item) => item.id}
                horizontal={true}
            />
        </SafeAreaView>
    );
};



export default Carousel;