import { configureStore } from '@reduxjs/toolkit';
import AuthReducer from '../Features/Auth/AuthSlice';
import ProductReducer from "../Features/Products/ProductsSlice";
import CustomerReducer from "../Features/Customers/CustomersSlice";

export default configureStore({
  reducer: {
    auth: AuthReducer,
    product: ProductReducer,
    customer: CustomerReducer,
  },
});
