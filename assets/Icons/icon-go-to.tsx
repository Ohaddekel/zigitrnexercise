import * as React from "react"
import Svg, { G, Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: title */

function SvgComponent(props) {
    return (
        <Svg
            width={12}
            height={11}
            viewBox="0 0 12 11"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <G
                stroke="#000"
                strokeWidth={1.048}
                fill="none"
                fillRule="evenodd"
                strokeLinecap="round"
            >
                <Path d="M10.95 5.235H.988M5.46.836L.988 5.235 5.46 9.633" />
            </G>
        </Svg>
    )
}

export default SvgComponent
