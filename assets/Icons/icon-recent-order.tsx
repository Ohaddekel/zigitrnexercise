import * as React from "react"
import Svg, { Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: title */

function SvgComponent(props) {
    return (
        <Svg
            width={16}
            height={16}
            viewBox="0 0 16 16"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                d="M8 7.053c.345 0 .623.28.623.623v3.29a.623.623 0 11-1.246 0v-3.29c0-.344.278-.623.623-.623zm0-2.224a.624.624 0 11-.002 1.25A.624.624 0 018 4.83zm0 9.924A6.76 6.76 0 011.247 8 6.761 6.761 0 018 1.247 6.761 6.761 0 0114.753 8 6.76 6.76 0 018 14.753zM8 0C3.586 0 0 3.589 0 8s3.586 8 8 8c4.41 0 8-3.589 8-8s-3.59-8-8-8z"
                fill="#C9D7DA"
                fillRule="evenodd"
            />
        </Svg>
    )
}

export default SvgComponent
