import * as React from "react"
import Svg, { Path } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: title */

function SvgComponent(props) {
    return (
        <Svg
            width={17}
            height={16}
            viewBox="0 0 17 16"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Path
                d="M11.916 1c2.134 0 3.84 1.755 4.06 4.175.016.107.087.669-.129 1.586-.31 1.323-1.029 2.526-2.076 3.479h0L8.455 15 3.23 10.24c-1.047-.953-1.765-2.156-2.076-3.48-.216-.916-.145-1.479-.128-1.586C1.244 2.755 2.95 1 5.085 1c1.399 0 2.647.753 3.374 2 .732-1.231 2.035-2 3.457-2z"
                fillRule="nonzero"
                stroke="#C9D7DA"
                strokeWidth={1.363}
                fill="none"
            />
        </Svg>
    )
}

export default SvgComponent
