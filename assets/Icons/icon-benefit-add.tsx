import * as React from "react"
import Svg, { G, Rect, Text, TSpan } from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: title */

function SvgComponent(props) {
    return (
        <Svg
            width={24}
            height={25}
            viewBox="0 0 24 25"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <G fill="none" fillRule="evenodd">
                <Rect fill="#000" y={1} width={24} height={24} rx={12} />
                <Text fillRule="nonzero" fontFamily="Heebo" fontSize={22} fill="#FFF">
                    <TSpan x={6} y={21}>
                        {"+"}
                    </TSpan>
                </Text>
            </G>
        </Svg>
    )
}

export default SvgComponent
