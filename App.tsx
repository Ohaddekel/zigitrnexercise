import React from 'react';
import { I18nextProvider } from 'react-i18next';
import i18n from './src/Services/Translation/index';
import store from "./src/Redux/store";
import { Provider } from 'react-redux';
import StackNavigation from "./src/Navigation/StackNavigation";
const App = () => {
  
  return (
    <Provider store={store}>
      <I18nextProvider i18n={i18n}>
        <StackNavigation />
      </I18nextProvider>
    </Provider>
  )
}

export default App;